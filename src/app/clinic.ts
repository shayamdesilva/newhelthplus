export class Clinic {

    constructor(
        public id: number,
        public createDate: Date,
        public registerNumber: string,
        public address: string,
        public clinicLogo: string,
        public active : boolean
    ){}
}
