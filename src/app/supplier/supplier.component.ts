import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Clinic } from '../clinic';
import { Doctor } from '../doctor';
import { EnrollmentService } from '../enrollment.service';
import { MedicineBrandSettingDTO } from '../medicine-brand-setting-dto';
import { MedicineGenericName } from '../medicine-generic-name';
import { SupplierDTO } from '../supplier-dto';
import { User } from '../user';

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.css']
})
export class SupplierComponent implements OnInit {
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  showLoadingIndicator = false
  dataSource;
  PeriodicElement:any=[];
  userDto = new User(null,'',null,null,'','',null,'',false,'','');
  doctorModel = new Doctor(null,'','','','','','','','','','','','','',null,null,false,null,false,null);
  clinicDto = new Clinic(null,null,'','','',false)
  displayedColumnsPharmacy: string[] = ['id','companyname','suppliername','phomenumber','email'];
  medicineGenericModel = new MedicineGenericName(null,null,null,'','');
  supplierModel = new SupplierDTO(null,'','','',null,null,'','','')
  medicineBrandSettingDto = new MedicineBrandSettingDTO(null,'','',null,null,'','');
  genericname:any[];
  constructor(private modalService: NgbModal,private http:HttpClient,private EnrollmentService:EnrollmentService
    ,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getLoggingUserById(localStorage.getItem("tokenid"));
  }

    onSubmit(){
      this.supplierModel.clinicId = this.clinicDto.id;
      this.supplierModel.userId = this.userDto.id;

      console.log(this.supplierModel.telephone);
      console.log(this.supplierModel.email);

      this.EnrollmentService.createSupplier(this.supplierModel)
    .subscribe(
      data => {
        if(data.status === 1){
          this._snackBar.open("Save Sucessfull", "", {
            duration: 2000,
          });
        }else{
          console.log(data.error )
          this._snackBar.open("Save faill", data.error['message'] ,{
            duration: 2000,
          });
        }
          
      }
    )

    }

    public getLoggingUserById(id){
      this.doctorModel.id = id;

      this.EnrollmentService.getClinicUserById(this.doctorModel)
    .subscribe(
      data => {
        this.userDto = data['userDTO'];
        this.clinicDto = data['clinicDTO'];
        console.log('Maintaince Data '+this.clinicDto)
         this.loadSupplierTable()
        // this.loadGenericName()
       
          
      }
    )

    }

    applyFilter(filterValue:string){
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    Clean(){
  
    }

    removeSupplier(element){

      this.EnrollmentService.deleteSupplier(element)
    .subscribe(
      data => {
        if(data.status === 1){
          this._snackBar.open("Delete Sucessfull", "", {
            duration: 2000,
          });
          this.loadSupplierTable()
        }else{
          console.log(data.error )
          this._snackBar.open("Save faill", data.error['message'] ,{
            duration: 2000,
          });
        }

          
      }
    )

    }
    editSupplier(element){ 
      this.supplierModel = element;
    }

    loadSupplierTable(){
      this.supplierModel.clinicId = this.clinicDto.id
    this.supplierModel.userId = this.userDto.id
    this.EnrollmentService.getAllSupplier(this.supplierModel)
    .subscribe(
      data => {


        this.PeriodicElement = data.supplierDTOs
    // console.log('Length '+ data.listMedicineGenericNameDTO.length)
     this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
     this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
    //  console.log('Datasource '+this.dataSource.length)

          
      }
    )

    }

}
