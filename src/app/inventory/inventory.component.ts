
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import * as alertify from 'alertifyjs'
import { trim } from 'jquery';
import { element } from 'protractor';

export interface PrescribeDespensaryElement {
  medicine: string;
  nooftime: string;
  day: string;
  qty: string;
  stockid:string;
  stockqty:number;
}

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
displayedColumnsPharmacy: string[] = ['stockid','medicine', 'nooftime','day','qty','stockqty'];
dataSource3;
ELEMENT_DATA4;
PrescribeDespensaryElement:any[];
  constructor(private modalService: NgbModal,private http:HttpClient) { 
    this.dataSource3 = this.ELEMENT_DATA4;


    this.PrescribeDespensaryElement = [
      {medicine:'Panadol',nooftime:'Morning',day:'5',qty:'20',stockid:'1',stockqty:80},
      {medicine:'Panadiene',nooftime:'Lunch',day:'50',qty:'200',stockid:'2',stockqty:30}
    ]
    this.dataSource3 = new MatTableDataSource(this.PrescribeDespensaryElement) ;
    this.dataSource3 = new MatTableDataSource(this.PrescribeDespensaryElement) ;

  }

  ngOnInit(): void {
  }

  SaveMedicine(){
    
  }



}
