export class DoctorToClinic {
    constructor(
        public id: number,
        public createDate:Date,
        public clinicId: string,
        public doctorId: string,
        public slmc: string,
        public email: string,
        public emailKey: string,
        public verifyDate: Date,
        public isEmailVerify:boolean
    ){}
}
