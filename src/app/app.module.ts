import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';

import { LoginpageComponent } from './loginpage/loginpage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AuthUserService } from 'src/services/auth-user.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs';
import {MatMenuModule} from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PatientComponent } from './patient/patient.component';
import { PatientregistrationComponent } from './patientregistration/patientregistration.component';
import { ManintenanceComponent } from './manintenance/manintenance.component';
import { InventoryComponent } from './inventory/inventory.component';
import { AddmedicineComponent } from './addmedicine/addmedicine.component';
import { AddgenericnameComponent } from './addgenericname/addgenericname.component';



import {A11yModule} from '@angular/cdk/a11y';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {OverlayModule} from '@angular/cdk/overlay';
import { SecurityComponent } from './security/security.component';
import { DoctorRegisterComponent } from './doctor-register/doctor-register.component';
import { SearchDoctorComponent } from './search-doctor/search-doctor.component';
import { CreateSystemUserComponent } from './create-system-user/create-system-user.component';
import { RegisterDoctorProfileComponent } from './register-doctor-profile/register-doctor-profile.component';
import { DoctorLoginComponent } from './doctor-login/doctor-login.component';
import { MedicineBrandSettingComponent } from './medicine-brand-setting/medicine-brand-setting.component';
import { SupplierComponent } from './supplier/supplier.component';
import { DrugTypeComponent } from './drug-type/drug-type.component';
import { MedicalBrandNameComponent } from './medical-brand-name/medical-brand-name.component';
import { PrescriptionCheckOutComponent } from './prescription-check-out/prescription-check-out.component';
import { ReceptionMaintainComponent } from './reception-maintain/reception-maintain.component';
import { StockReportComponent } from './stock-report/stock-report.component';






const appRoutes:Routes=[
  {path:'', component:LoginpageComponent},
{path:'user/login', component:LoginpageComponent},
{path:'dashboard', component:DashboardComponent},
{path:'maintenance', component:ManintenanceComponent},
{path:'patientregistration', component:PatientregistrationComponent},
{path:'inventory', component:InventoryComponent},
{path:'addmedicine', component:AddmedicineComponent},
{path:'addgenericname', component:AddgenericnameComponent},
{path:'searchdoctor', component:SearchDoctorComponent},
{path:'createsystemuser', component:CreateSystemUserComponent},
{path:'registerdoctorprofile', component:RegisterDoctorProfileComponent},
{path:'doctorlogin', component:DoctorLoginComponent},
{path:'brandsetting', component:MedicineBrandSettingComponent},
{path:'supplier', component:SupplierComponent},
{path:'drugtype', component:DrugTypeComponent},
{path:'medicalbrandname', component:MedicalBrandNameComponent},
{path:'prescriptioncheckout', component:PrescriptionCheckOutComponent},
{path:'receptionmaintenance', component:ReceptionMaintainComponent},
{path:'stock', component:StockReportComponent},

]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LeftMenuComponent,
    LoginpageComponent,
    LeftMenuComponent,
    DashboardComponent,
    PatientComponent,
    PatientregistrationComponent,
    ManintenanceComponent,
    InventoryComponent,
    AddmedicineComponent,
    AddgenericnameComponent,
    SecurityComponent,
    DoctorRegisterComponent,
    SearchDoctorComponent,
    CreateSystemUserComponent,
    RegisterDoctorProfileComponent,
    DoctorLoginComponent,
    MedicineBrandSettingComponent,
    SupplierComponent,
    DrugTypeComponent,
    MedicalBrandNameComponent,
    PrescriptionCheckOutComponent,
    ReceptionMaintainComponent,
    StockReportComponent,
  ],
  imports: [
    ClipboardModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    OverlayModule,
    ScrollingModule,
    PortalModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    NgbModule,
    MatMenuModule,
    MatSidenavModule,
    MatButtonModule,
    MatListModule,
    TabsModule.forRoot(),
    BrowserAnimationsModule,
    MatExpansionModule,
    MatSortModule,
    MatTableModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    A11yModule


  ],
  providers: [
    AuthUserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
