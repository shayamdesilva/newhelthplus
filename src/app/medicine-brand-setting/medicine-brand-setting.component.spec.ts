import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicineBrandSettingComponent } from './medicine-brand-setting.component';

describe('MedicineBrandSettingComponent', () => {
  let component: MedicineBrandSettingComponent;
  let fixture: ComponentFixture<MedicineBrandSettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicineBrandSettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicineBrandSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
