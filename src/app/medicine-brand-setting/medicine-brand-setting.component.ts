import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Clinic } from '../clinic';
import { Doctor } from '../doctor';
import { EnrollmentService } from '../enrollment.service';
import { MedicineBrandSettingDTO } from '../medicine-brand-setting-dto';
import { MedicineGenericName } from '../medicine-generic-name';
import { User } from '../user';

@Component({
  selector: 'app-medicine-brand-setting',
  templateUrl: './medicine-brand-setting.component.html',
  styleUrls: ['./medicine-brand-setting.component.css']
})
export class MedicineBrandSettingComponent implements OnInit {
  
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  showLoadingIndicator = false
  dataSource;
  PeriodicElement:any=[];
  genericname:any[];
  userDto = new User(null,'',null,null,'','',null,'',false,'','');
  doctorModel = new Doctor(null,'','','','','','','','','','','','','',null,null,false,null,false,null);
  clinicDto = new Clinic(null,null,'','','',false)
  displayedColumnsPharmacy: string[] = ['stockid','medicine','sellingPrice','minimumQty','createDate',];
  medicineGenericModel = new MedicineGenericName(null,null,null,'','');
  medicineBrandSettingDto = new MedicineBrandSettingDTO(null,'','',null,null,'','');

  constructor(private modalService: NgbModal,private http:HttpClient,private EnrollmentService:EnrollmentService
    ,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getLoggingUserById(localStorage.getItem("tokenid"));
    
    
  }

  onSubmit(){
    console.log( this.userDto.id)
    console.log(this.clinicDto.id)
    console.log(this.medicineBrandSettingDto.minimumQty)
    this.medicineBrandSettingDto.clinicId = this.clinicDto.id;
    this.medicineBrandSettingDto.userId = this.userDto.id;


    this.EnrollmentService.createMedicineBrasndSetting(this.medicineBrandSettingDto)
    .subscribe(
      data => {
        if(data.status === 1){
          this._snackBar.open("Save Sucessfull", "", {
            duration: 2000,
          });
        }else{
          console.log(data.error )
          this._snackBar.open("Save faill", data.error['message'] ,{
            duration: 2000,
          });
        }

          
      }
    )

  }
  
  applyFilter(filterValue:string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  Clean(){

  }

  removeMedicine(element){

    this.EnrollmentService.deleteMedicineBrasndSetting(element)
    .subscribe(
      data => {
        if(data.status === 1){
          this._snackBar.open("Delete Sucessfull", "", {
            duration: 2000,
          });
          this.loadMedicineTable()
        }else{
          console.log(data.error )
          this._snackBar.open("Save faill", data.error['message'] ,{
            duration: 2000,
          });
        }

          
      }
    )

  }

  public getLoggingUserById(id){
    this.doctorModel.id = id;

        this.EnrollmentService.getClinicUserById(this.doctorModel)
    .subscribe(
      data => {
        this.userDto = data['userDTO'];
        this.clinicDto = data['clinicDTO'];
        console.log('Maintaince Data '+this.clinicDto.id)
        this.loadMedicineTable()
        this.loadGenericName()
       
          
      }
    )
  


  }

  editMedicine(element){
    console.log(element)
    this.medicineBrandSettingDto = element;
  }

  loadGenericName(){
    this.EnrollmentService.getAllGenericNameByClinicId(this.medicineGenericModel)
    .subscribe(
      data => {

        this.genericname = data.listMedicineGenericNameDTO

          
      }
    )
  }

  loadMedicineTable(){
    this.medicineGenericModel.clinicId = this.clinicDto.id
    this.medicineGenericModel.userId = this.userDto.id
    console.log('this.medicineGenericModel.clinicId==>> '+this.clinicDto.id)
    this.EnrollmentService.getAllMedicineNameByClinicId(this.medicineGenericModel)
    .subscribe(
      data => {


        this.PeriodicElement = data.medicineBrandSettingDTOs
    // console.log('Length '+ data.listMedicineGenericNameDTO.length)
     this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
     this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
    //  console.log('Datasource '+this.dataSource.length)

          
      }
    )

  }
}
