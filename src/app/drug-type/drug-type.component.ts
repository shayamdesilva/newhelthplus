import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Clinic } from '../clinic';
import { Doctor } from '../doctor';
import { DrugTypeDTO } from '../drug-type-dto';
import { EnrollmentService } from '../enrollment.service';
import { MedicineBrandSettingDTO } from '../medicine-brand-setting-dto';
import { MedicineGenericName } from '../medicine-generic-name';
import { User } from '../user';

@Component({
  selector: 'app-drug-type',
  templateUrl: './drug-type.component.html',
  styleUrls: ['./drug-type.component.css']
})
export class DrugTypeComponent implements OnInit {
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  showLoadingIndicator = false
  dataSource;
  PeriodicElement:any=[];
  drugImage:any[];
  userDto = new User(null,'',null,null,'','',null,'',false,'','');
  doctorModel = new Doctor(null,'','','','','','','','','','','','','',null,null,false,null,false,null);
  clinicDto = new Clinic(null,null,'','','',false)
  displayedColumnsPharmacy: string[] = ['stockid','drugtype',];
  medicineGenericModel = new MedicineGenericName(null,null,null,'','');
  medicineBrandSettingDto = new MedicineBrandSettingDTO(null,'','',null,null,'','');
drugTypeDto = new DrugTypeDTO(null,'',null,'',null,null,'');
  constructor(private modalService: NgbModal,private http:HttpClient,private EnrollmentService:EnrollmentService
    ,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getLoggingUserById(localStorage.getItem("tokenid"));
    this.loadingImage()
  }
  onSubmit(){
    this.drugTypeDto.clinicId = this.clinicDto.id;
    this.drugTypeDto.userId = this.userDto.id;


    this.EnrollmentService.createDrugType(this.drugTypeDto)
    .subscribe(
      data => {
        if(data.status === 1){
          this._snackBar.open("Save Sucessfull", "", {
            duration: 2000,
          });
          this.loadMedicineTable()
        }else{
          console.log(data.error )
          this._snackBar.open("Save faill", data.error['message'] ,{
            duration: 2000,
          });
        }

          
      }
    )
  }

  applyFilter(filterValue:string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  Clean(){

  }

  removeMedicine(element){
    this.EnrollmentService.deleteDrugType(element)
    .subscribe(
      data => {
        if(data.status === 1){
          this._snackBar.open("Delete Sucessfull", "", {
            duration: 2000,
          });
          this.loadMedicineTable()
        }else{
          console.log(data.error )
          this._snackBar.open("Save faill", data.error['message'] ,{
            duration: 2000,
          });
        }

          
      }
    )
  }

  public getLoggingUserById(id){
    this.doctorModel.id = id;

    this.EnrollmentService.getClinicUserById(this.doctorModel)
.subscribe(
  data => {
    this.userDto = data['userDTO'];
    this.clinicDto = data['clinicDTO'];
    console.log('Maintaince Data '+this.clinicDto.id)
    this.loadMedicineTable()
   
      
  }
)
  }

  editMedicine(element){
    console.log(element)
    this.drugTypeDto = element;
  }

  loadingImage(){

    this.EnrollmentService.getAllDrugTypeImage(this.drugTypeDto)
    .subscribe(
      data => {
        this.drugImage = data.drugTypeImageDTOs;



     console.log('Datasource length '+data.drugTypeImageDTOs)

          
      }
    )

  }

  loadMedicineTable(){
    this.drugTypeDto.clinicId = this.clinicDto.id
    this.drugTypeDto.userId = this.userDto.id
    console.log('this.medicineGenericModel.clinicId==>> '+this.clinicDto.id)
    this.EnrollmentService.getAllDrugType(this.drugTypeDto)
    .subscribe(
      data => {


        this.PeriodicElement = data.drugTypeDTOs
    // console.log('Length '+ data.listMedicineGenericNameDTO.length)
     this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
     this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
    //  console.log('Datasource '+this.dataSource.length)

          
      }
    )
  }

}
