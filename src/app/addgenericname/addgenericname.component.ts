
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import * as alertify from 'alertifyjs'
import { trim } from 'jquery';
import { element } from 'protractor';
import { MedicineGenericName } from '../medicine-generic-name';
import { User } from '../user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EnrollmentService } from '../enrollment.service';
import { Doctor } from '../doctor';
import { Clinic } from '../clinic';

export interface PeriodicElement  {
  medicine: string;
}

@Component({
  selector: 'app-addgenericname',
  templateUrl: './addgenericname.component.html',
  styleUrls: ['./addgenericname.component.css']
})
export class AddgenericnameComponent implements OnInit {
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
displayedColumnsPharmacy: string[] = ['stockid','medicine'];
dataSource3;
userDto = new User(null,'',null,null,'','',null,'',false,'','');
doctorModel = new Doctor(null,'','','','','','','','','','','','','',null,null,false,null,false,null);
clinicDto = new Clinic(null,null,'','','',false)
dataSource;
ELEMENT_DATA;
medicineGenericModel = new MedicineGenericName(null,null,null,'','');
PrescribeDespensaryElement:any=[];
PeriodicElement:any=[];
devisionTabTable=[];
  constructor(private modalService: NgbModal,private http:HttpClient,private EnrollmentService:EnrollmentService
    ,private _snackBar: MatSnackBar) { 
     this.dataSource = this.ELEMENT_DATA;
    //  this.PeriodicElement.push(
    //   {medicine:'Paracetamol'},
    //   {medicine:'Nafcillin'},
    //   {medicine:'Acetic Acid'},
    //   {medicine:'Amoxicillin'},
    //   {medicine:'Cloxacillin'},
    //   {medicine:'Cloxacillin'},
    // )

    // console.log('PrescribeDespensaryElement'+ this.PeriodicElement.length)
    // this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    // console.log('Datasource '+this.dataSource.length)

     this.loadTable();


  }

  ngOnInit(): void {
    this.getLoggingUserById(localStorage.getItem("tokenid"));
    // this.loadMedicineTable()




  }

  loadTable(){
    // this.PeriodicElement.push(
    //   {medicine:'Paracetamol'},
    //   {medicine:'Nafcillin'},
    //   {medicine:'Acetic Acid'},
    //   {medicine:'Amoxicillin'},
    //   {medicine:'Cloxacillin'},
    //   {medicine:'Cloxacillin'},
    // )

    // console.log('PrescribeDespensaryElement'+ this.PeriodicElement.length)
    //  this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
    //  this.dataSource.paginator = this.paginator;
    //  this.dataSource.sort = this.sort;
    //  console.log('Datasource '+this.dataSource.length)

    
  }


  public getLoggingUserById(id){
    this.doctorModel.id = id;

        this.EnrollmentService.getClinicUserById(this.doctorModel)
    .subscribe(
      data => {
       
        this.userDto = data['userDTO'];
        this.clinicDto = data['clinicDTO'];
        console.log('Maintaince Data '+this.clinicDto.id)
        if(data.status === 1)
        this.loadMedicineTable()
          
      }
    )


  }

  loadMedicineTable(){
    this.medicineGenericModel.clinicId = this.clinicDto.id
    this.medicineGenericModel.userId = this.userDto.id
    console.log('this.medicineGenericModel.clinicId==>> '+this.clinicDto.id)
    this.EnrollmentService.getAllMedicineNameByClinicId(this.medicineGenericModel)
    .subscribe(
      data => {

    //         this.PeriodicElement.push(
    //   {medicine:'Paracetamol'},
    //   {medicine:'Nafcillin'},
    //   {medicine:'Acetic Acid'},
    //   {medicine:'Amoxicillin'},
    //   {medicine:'Cloxacillin'},
    //   {medicine:'Cloxacillin'},
    // )


    this.PeriodicElement = data.listMedicineGenericNameDTO
    console.log('Length '+ data.listMedicineGenericNameDTO.length)
     this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
     this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
     console.log('Datasource '+this.dataSource.length)

          
      }
    )

  }
  SaveMedicine(){
    console.log(this.medicineGenericModel)
    console.log('Clinic '+this.clinicDto.id)
    console.log('User '+this.userDto.id)

    this.medicineGenericModel.clinicId = this.clinicDto.id
    this.medicineGenericModel.userId = this.userDto.id

    this.EnrollmentService.createMedicineGenericName(this.medicineGenericModel)
    .subscribe(
      data => {
        if(data['status'] === 1){
          this._snackBar.open("Saved", "Medicine Saved", {
            duration: 2000,
          });
        }else{
          this._snackBar.open("Faill Save", data['error']['message'], {
            duration: 2000,
          });
        }

        this.PeriodicElement = data['listMedicineGenericNameDTO']
          
      }
    )
    

    console.log('PrescribeDespensaryElement'+ this.PeriodicElement.length)
     this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
     this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
     console.log('Datasource '+this.dataSource.length)

  }

  applyFilter(filterValue:string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  removeMedicine(element){
    this.medicineGenericModel = element;
    this.EnrollmentService.deleteMedicineNameById(this.medicineGenericModel)
    .subscribe(
      data => {
        if(data['status'] === 1){
          this.loadMedicineTable()
          this._snackBar.open("Removed", "Deleted", {
            duration: 2000,
          });
        }else{
          this._snackBar.open("Faill Save", data['error']['message'], {
            duration: 2000,
          });
        }

       
          
      }
    )
 
  }

  
  editMedicine(element){
    console.log(element)
    this.medicineGenericModel = element;
  }

}
