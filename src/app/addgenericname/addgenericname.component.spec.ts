import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddgenericnameComponent } from './addgenericname.component';

describe('AddgenericnameComponent', () => {
  let component: AddgenericnameComponent;
  let fixture: ComponentFixture<AddgenericnameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddgenericnameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddgenericnameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
