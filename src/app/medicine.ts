export class Medicine {

    constructor(
        public id: number,
        public medicine: string,
        public dose: string,
        public frequency: string,
        public day: string,
        public qty: string,
    ){}
}
