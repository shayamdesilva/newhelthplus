import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Clinic } from '../clinic';
import { Doctor } from '../doctor';
import { EnrollmentService } from '../enrollment.service';
import { MedicalBrandNameDTO } from '../medical-brand-name-dto';
import { MedicineGenericName } from '../medicine-generic-name';
import { User } from '../user';
import { MedicineBrandSettingDTO } from '../medicine-brand-setting-dto';

@Component({
  selector: 'app-stock-report',
  templateUrl: './stock-report.component.html',
  styleUrls: ['./stock-report.component.css']
})
export class StockReportComponent implements OnInit {
  showLoadingIndicator = false;
  userDto = new User(null,'',null,null,'','',null,'',false,'','');
  doctorModel = new Doctor(null,'','','','','','','','','','','','','',null,null,false,null,false,null);
  clinicDto = new Clinic(null,null,'','','',false)
  medicineGenericModel = new MedicineGenericName(null,null,null,'','');
  brandSetting:any=[];
  medicineBrandNameDto = new MedicalBrandNameDTO(null,null,null,'',null,null,null,null,'',null,null,null,null,'');
  medicineBrandSettingDto = new MedicineBrandSettingDTO(null,'','',null,null,'','');
  pdfResult:string;
  pdfData;
  today:Date;



  constructor(private http:HttpClient,private EnrollmentService:EnrollmentService
    ,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getLoggingUserById(localStorage.getItem("tokenid"));
  }

  onSubmit(){
    console.log(this.medicineBrandNameDto.medicineBrandSettingId)
    this.EnrollmentService.getDrugsBalanceById(this.medicineBrandNameDto)
    .subscribe(
      data => {
        // console.log('Maintaince Data '+data.balanceDTOs[0]['insertBrandName'])
        this.pdfData = data.balanceDTOs[0];
        this.loadPrintDate("Stock Report",)
      }
    )

  }

  public getLoggingUserById(id){
    this.doctorModel.id = id;

    this.EnrollmentService.getClinicUserById(this.doctorModel)
.subscribe(
  data => {
    this.userDto = data['userDTO'];
    this.clinicDto = data['clinicDTO'];
    console.log('Maintaince Data '+this.clinicDto.id)
    this.loadBrandName() 
  }
)
  }

  loadBrandName(){
    this.medicineGenericModel.clinicId = this.clinicDto.id
    this.medicineGenericModel.userId = this.userDto.id
    this.EnrollmentService.getAllMedicineNameByClinicId(this.medicineGenericModel)
    .subscribe(
      data => {
        this.medicineBrandSettingDto.id = 888
        this.medicineBrandSettingDto.brandName = "All Medicine"
        // this.brandSetting.push(this.medicineBrandSettingDto)
        data.medicineBrandSettingDTOs.push(this.medicineBrandSettingDto)
        this.brandSetting = data.medicineBrandSettingDTOs
      }
    )

  }

  
  loadPrintDate(data){
// this.pdfData = data;
    this.pdfResult = '<div class="container">';

    this.pdfResult = this.pdfResult + '<div class="row" >'+
    // '<table>'+
    // '<tbody>'+
    // ' <tr>'+
    // '<td><img src="assets/images/logo.png" width="100px" height="50px" ></td>'+
    // '<td style="padding-left:150px; font-size: 10px; font-weight: bolder;" onload="loadPrintPDF()"> AAAAAAA </td> '+
    // '<td style="padding-left:500px"> BBBBBB <br /><img src="assets/images/barcode.png" width="50px" height="20px" ></td> '+
    // '</tr>'+
    // '</tbody>'+
    // '</table>'+
    '</div>'+
    ' <div class="row">'+
    '<table>'+
    ' <tr>'+
    '<td>'+
    '<div id="bloc1"> <div style="width:500px;height:100px;border:1px solid #000; "><span style="margin-left: 50px ; font-size: 15px;">TERRYS SPARES </span>'+
    '<br/> <br/> <br/><span style="margin-left: 50px ; font-size: 15px;">Phone No: 03-7461-3843 </span>'+
    '</div></div>'+
    '</td>'+
    '<td>'+
    '<div id="bloc2"> <div style="width:500px;height:100px;border:1px solid #000; ">'+
    '<span style="margin-left: 50px ; font-size: 15px;">164 CENTRAL PARK DRIVE </span>'+
    '<br/> <span style="margin-left: 50px ; font-size: 15px;">RICHMOND </span>'+
    '<br/> <span style="margin-left: 50px ; font-size: 15px;">MELBOURNE </span>'+
    '<br/> <span style="margin-left: 50px ; font-size: 15px;">AUSTRALIA </span>'+
    '</div></div>'+
    '</td>'+
    '</tr>'+
    '</table> '+
    ' </div>'

    // console.log("Pinnting Data..... "+this.pdfData[0]['brandId']);
    var innerContents = this.pdfResult;
    var popupWinindow = window.open('', '', 'width=1000,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWinindow.document.open();
    popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + 
    
    innerContents
    
    + '</html>');
    popupWinindow.document.close();

    console.log("Enf PDF");


  }
}
