export class Allergy {
    constructor(
        public id:number,
        public createDate:Date,
        public patientId: number,
        public allergyDescription: string,
    ){}
}
