import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  logUserName:String;
  userCompanyName:String;
  constructor(private router:Router) {
    console.log('constructor : '+localStorage.getItem('token'));


  }


  ngOnInit(): void {
    if(localStorage.getItem('token')){
      if(localStorage.getItem('token').includes('@')){
        var logUsers:String[] = localStorage.getItem('token').split('@');
        this.logUserName = logUsers[0];
        var CompanyName:String = logUsers[1].split('.')[0];
        this.userCompanyName = CompanyName.charAt(0).toUpperCase()+ CompanyName.slice(1);
      }else{
        this.logUserName = localStorage.getItem('token');
      }


       
       console.log("User name : "+this.logUserName);

    }

  }

  loggedIn() {
    return this.logUserName;
  }

  onLogout(){
    localStorage.removeItem('token');
    this.router.navigate(['/home']);
  }

}
