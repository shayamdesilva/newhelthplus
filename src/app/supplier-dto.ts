export class SupplierDTO {

    constructor(
        public id: number,
        public supplierName: string,
        public address: string,
        public createdDate: String,
        public clinicId: number,
        public userId: number,
        public companyName:string,
        public email:string,
        public telephone:string,
    ){}
}
