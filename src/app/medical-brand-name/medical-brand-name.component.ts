
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import * as alertify from 'alertifyjs'
import { trim } from 'jquery';
import { element } from 'protractor';
import { User } from '../user';
import { Doctor } from '../doctor';
import { Clinic } from '../clinic';
import { MedicineGenericName } from '../medicine-generic-name';
import { MedicineBrandSettingDTO } from '../medicine-brand-setting-dto';
import { DrugTypeDTO } from '../drug-type-dto';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EnrollmentService } from '../enrollment.service';
import { MedicalBrandNameDTO } from '../medical-brand-name-dto';
import { SupplierDTO } from '../supplier-dto';

@Component({
  selector: 'app-medical-brand-name',
  templateUrl: './medical-brand-name.component.html',
  styleUrls: ['./medical-brand-name.component.css']
})
export class MedicalBrandNameComponent implements OnInit {
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  showLoadingIndicator = false
  dataSource;
  PeriodicElement:any=[];
  brandSetting:any=[];
  supplyer:any=[];
  drugTypes:any=[];
  genericname:any[];
  userDto = new User(null,'',null,null,'','',null,'',false,'','');
  doctorModel = new Doctor(null,'','','','','','','','','','','','','',null,null,false,null,false,null);
  clinicDto = new Clinic(null,null,'','','',false)
  displayedColumnsPharmacy: string[] = ['stockid','strength','noofunit','purchacePrice','createDate'];
  medicineGenericModel = new MedicineGenericName(null,null,null,'','');
  medicineBrandSettingDto = new MedicineBrandSettingDTO(null,'','',null,null,'','');
  medicineBrandNameDto = new MedicalBrandNameDTO(null,null,null,'',null,null,null,null,'',null,null,null,null,'');
  supplierModel = new SupplierDTO(null,'','','',null,null,'','','')
  drugTypeDto = new DrugTypeDTO(null,'',null,'',null,null,'');

  constructor(private modalService: NgbModal,private http:HttpClient,private EnrollmentService:EnrollmentService
    ,private _snackBar: MatSnackBar) { 
  

  }

  applyFilter(filterValue:string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  Clean(){

  }

  removeMedicine(element){
    this.EnrollmentService.deleteMedicineBrandName(element)
    .subscribe(
      data => {
        if(data.status === 1){
          this._snackBar.open("Delete Sucessfull", "", {
            duration: 2000,
          });
          this.loadMedicineTable()
        }else{
          console.log(data.error )
          this._snackBar.open("Save faill", data.error['message'] ,{
            duration: 2000,
          });
        }

          
      }
    )
  }

  ngOnInit(): void {
    this.getLoggingUserById(localStorage.getItem("tokenid"));
  }

  public getLoggingUserById(id){
    this.doctorModel.id = id;

    this.EnrollmentService.getClinicUserById(this.doctorModel)
.subscribe(
  data => {
    this.userDto = data['userDTO'];
    this.clinicDto = data['clinicDTO'];
    console.log('Maintaince Data '+this.clinicDto.id)
    this.loadMedicineTable()
    this.loadBrandName()
    this.loadSupplier()
    this.loadDrugType()

   
      
  }

  
)
  }

  loadDrugType(){
    this.drugTypeDto.clinicId = this.clinicDto.id
    this.drugTypeDto.userId = this.userDto.id
    console.log('this.medicineGenericModel.clinicId==>> '+this.clinicDto.id)
    this.EnrollmentService.getAllDrugType(this.drugTypeDto)
    .subscribe(
      data => {
        this.drugTypes = data.drugTypeDTOs
      }
    )
  }

  loadBrandName(){
    this.medicineGenericModel.clinicId = this.clinicDto.id
    this.medicineGenericModel.userId = this.userDto.id
    this.EnrollmentService.getAllMedicineNameByClinicId(this.medicineGenericModel)
    .subscribe(
      data => {
        this.brandSetting = data.medicineBrandSettingDTOs
      }
    )

  }

  loadSupplier(){
    this.supplierModel.clinicId = this.clinicDto.id
    this.supplierModel.userId = this.userDto.id
    this.EnrollmentService.getAllSupplier(this.supplierModel)
    .subscribe(
      data => {
        this.supplyer = data.supplierDTOs
      }
    )

  }


  editMedicine(element){
    console.log(element)
    this.medicineBrandNameDto = element;
  }

  loadMedicineTable(){
    this.medicineBrandNameDto.clinicId = this.clinicDto.id
    this.medicineBrandNameDto.userId = this.userDto.id
    console.log('this.medicineGenericModel.clinicId==>> '+this.clinicDto.id)
    this.EnrollmentService.getAllMedicineBrandName(this.medicineBrandNameDto)
    .subscribe(
      data => {


        this.PeriodicElement = data.medicineBrandNameDTOs
    console.log('Length '+ this.PeriodicElement.length)
     this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
     this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
    //  console.log('Datasource '+this.dataSource.length)

          
      }
    )

  }

  
  onSubmit(){
    console.log( this.userDto.id)
    console.log(this.clinicDto.id)
    console.log(this.medicineBrandSettingDto.minimumQty)
    this.medicineBrandNameDto.clinicId = this.clinicDto.id;
    this.medicineBrandNameDto.userId = this.userDto.id;


    this.EnrollmentService.createMedicineBrandName(this.medicineBrandNameDto)
    .subscribe(
      data => {
        if(data.status === 1){
          this._snackBar.open("Save Sucessfull", "", {
            duration: 2000,
          });
          this.PeriodicElement = data.medicineBrandNameDTOs
          console.log('Length '+ this.PeriodicElement.length)
           this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
           this.dataSource.paginator = this.paginator;
           this.dataSource.sort = this.sort;
        }else{
          console.log(data.error )
          this._snackBar.open("Save faill", data.error['message'] ,{
            duration: 2000,
          });
        }

          
      }
    )

  }
  
  SaveMedicine(){
    
  }


}
