import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalBrandNameComponent } from './medical-brand-name.component';

describe('MedicalBrandNameComponent', () => {
  let component: MedicalBrandNameComponent;
  let fixture: ComponentFixture<MedicalBrandNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalBrandNameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalBrandNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
