import { DecimalPipe } from "@angular/common";

export class Fee {

    constructor(
        public id: number,
        public patientId:number,
        public createDate:Date,
        public amount: string,
        public ispaid:boolean
    ){}
}
