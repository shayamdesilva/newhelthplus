import { Component, OnInit } from '@angular/core';
import { Console } from 'console';
import { JwtClientService } from '../jwt-client.service';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.css']
})
export class SecurityComponent implements OnInit {

  authRequest:any={
    "email":"thuni@gmail.com",
    "usrPasswrd":"123"
}

  constructor( private service:JwtClientService) { }

  ngOnInit(): void {
  }

  public getAccessToken(authRequest){
    let resp = this.service.generateToken(authRequest);
    resp.subscribe(data=>console.log("Token ===> "+data))

  }

}
