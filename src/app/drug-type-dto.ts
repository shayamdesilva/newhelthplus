export class DrugTypeDTO {

    constructor(
        public id: number,
        public type:string,
        public createDate:Date,
        public imageId: string,
        public userId: number,
        public clinicId: number,
        public description:string
    ){}
}
