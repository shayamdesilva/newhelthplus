import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EnrollmentService } from '../enrollment.service';
import { Patient } from '../patient';

@Component({
  selector: 'app-prescription-check-out',
  templateUrl: './prescription-check-out.component.html',
  styleUrls: ['./prescription-check-out.component.css']
})
export class PrescriptionCheckOutComponent implements OnInit {
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  dataSource;  
  displayedColumns: string[] = ['Date', 'Presentedcomplain' ,'Examination', 'Precription'];
  patientModel = new Patient(null,'','','','','','','','','','',false,'',false,'',false,null,null,'','','','','');
  PeriodicElement:any=[];
  constructor(private modalService: NgbModal,private http:HttpClient,private EnrollmentService:EnrollmentService
    ,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }


  applyFilter(filterValue:string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public setPatientModel(model){
    console.log(model);
    // this.doctorModel = model;
    // console.log('User Id : '+this.doctorModel.id);
    // console.log('User Id : '+this.doctorModel.firstName);
    // console.log('User Id : '+this.doctorModel.lastName);
    this.loadPatientDrugCheckOut(model)

  }
  loadPatientDrugCheckOut(model){
    this.patientModel = model;
    this.EnrollmentService.enrollGetDiagnosisPatientsDetails(this.patientModel)
    .subscribe(
      data => {
	          this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
    this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
	  }
	  )
  }


}
