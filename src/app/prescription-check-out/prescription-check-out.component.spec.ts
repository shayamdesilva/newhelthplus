import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescriptionCheckOutComponent } from './prescription-check-out.component';

describe('PrescriptionCheckOutComponent', () => {
  let component: PrescriptionCheckOutComponent;
  let fixture: ComponentFixture<PrescriptionCheckOutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrescriptionCheckOutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescriptionCheckOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
