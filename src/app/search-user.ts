export class SearchUser {

    constructor(
        public firstName: string,
        public lastName: string,
        public dateOfBirth: Date,
        public doctorId:string,
        public doctorSlmc:string,
        public clinicId:string


    ){}
}
