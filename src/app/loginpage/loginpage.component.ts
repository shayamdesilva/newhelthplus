import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { AuthUserService } from 'src/services/auth-user.service';
import { Router } from '@angular/router';
 import * as alertify from 'alertifyjs'
import { JwtClientService } from '../jwt-client.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Login } from '../login';
import { EnrollmentService } from '../enrollment.service';
import { User } from '../user';
import { Doctor } from '../doctor';

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})
export class LoginpageComponent implements OnInit {
@ViewChild('Form') addPropertyForm: NgForm
userLogin = new Login('','');
doctorModel = new Doctor(null,'','','','','','','','','','','','','',null,null,false,null,false,null);
jobCardarray:any =[];
userDto:any=[];
authRequest:any=[];
authRequest1:any=[];
userLoginData:string;

  constructor(private httpClient: HttpClient,
    private authservices:AuthUserService,
    private router:Router,private serv:JwtClientService,
    private _snackBar: MatSnackBar,
    private EnrollmentService:EnrollmentService
    ) { }

  ngOnInit(): void {
    // this.getAllUser().subscribe(data => {
    //   console.log(data);
    // });
    this.jobCardarray.push({email:'thuni@gmail.com',usrPasswrd:'123'})

  }

  //Submit button clicked
  onSubmit(Form :NgForm){
    //Call api and iterate the response
    console.log('Login PW : '+this.userLogin.password);
    console.log('Login username : '+this.userLogin.userName);
    this.doctorModel.userName = this.userLogin.userName.toString();
    this.doctorModel.password = this.userLogin.password.toString();

    this.EnrollmentService.userLogin(this.doctorModel)
    .subscribe(
      data => {
        if(data.status === 1  && data.httpCode === 200 ){
          // console.log('Status '+data.status);
          this._snackBar.open("Loggin", "Login Sucefull", {
            duration: 2000,
          });
          // console.log(data.userDTos[0]['id'])

          localStorage.setItem('token',data['userDTO']['firstName']);
          localStorage.setItem('tokenid',data['userDTO']['id']);
          localStorage.setItem('clinicId',data['userDTO']['clinicId']);
          // if(data['rollDTO']['roleName'])
          // localStorage.setItem('slmc',data.userDTos[0]['slmcNumber']);
          // localStorage.setUser('user',data.userDTos[0]);
          // this.maintaince.setUserModel(data.userDTos[0]);
          this.router.navigate(['/maintenance']);

        }else{
          console.log('Status '+data.status);
          this._snackBar.open("Loggin Fail", "Incorrect username or password", {
            duration: 2000,
          });
        }

          
      }
    )


    // this.userDto['email']=this.addPropertyForm.value.username;
    // this.userDto['password']=this.addPropertyForm.value.password;
    // this.authRequest.push({'email':this.addPropertyForm.value.username,'usrPasswrd':this.addPropertyForm.value.password})

    // this.authRequest1={
    //   "email":this.addPropertyForm.value.username,
    //   "usrPasswrd":this.addPropertyForm.value.password
    // }

    // console.log('authRequest '+this.authRequest)
    // console.log('authRequest1 '+this.authRequest1)



// this.httpClient.post('http://localhost:8080/api/users/login', this.authRequest1) .subscribe(
//   res => {
//     console.log('Token = '+res['message']);
//   if(res['message'] === 'Login sucess'){
//     console.log('Token = '+res['message']);
//     localStorage.setItem('jwttoken',res['message'])
//     this._snackBar.open(res['message'], 'OK', {
//       duration: 3000
//     });
//     this.router.navigate(['/dashboard']);

//   }else{
//     console.log('Invalid Token...');
//     this._snackBar.open(res['message'], 'OK', {
//       duration: 3000
//     });
//   }
// }
//   );

  }


}
