
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import * as alertify from 'alertifyjs'
import { trim } from 'jquery';
import { element } from 'protractor';

export interface PrescribeDespensaryElement {
  medicine: string;
}

@Component({
  selector: 'app-addmedicine',
  templateUrl: './addmedicine.component.html',
  styleUrls: ['./addmedicine.component.css']
})
export class AddmedicineComponent implements OnInit {
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  panelOpenState = false;
displayedColumnsPharmacy: string[] = ['id','genericname','brandname','drugtype','supplier','drugstrength','unit','unitprice','mimimumqty','manufacturedate','expirydate'];
dataSource3;
ELEMENT_DATA4;
PrescribeDespensaryElement:any=[];
devisionTabTable=[];
  constructor(private modalService: NgbModal,private http:HttpClient) { 
    this.dataSource3 = this.ELEMENT_DATA4;



  }

  ngOnInit(): void {
    this.PrescribeDespensaryElement.push(
      {genericname:'Paracetamol',brandname:'Panadol',drugtype:'Tablet',supplier:'SSL',drugstrength:'2.5',unit:'mg',unitprice:'5.00',mimimumqty:2000,manufacturedate:'2020-01-10',expirydate:'2025-10-27'},
      {genericname:'Paracetamol',brandname:'Hedex',drugtype:'Tablet',supplier:'SSL',drugstrength:'0.5',unit:'mg',unitprice:'150.00',mimimumqty:2000,manufacturedate:'2019-01-10',expirydate:'2028-10-27'},
      {genericname:'Paracetamol',brandname:'Disprol',drugtype:'Syrup',supplier:'SSL',drugstrength:'2.5',unit:'ml',unitprice:'250.00',mimimumqty:500,manufacturedate:'2019-03-10',expirydate:'2023-12-27'},
      {genericname:'Paracetamol',brandname:'Calpol',drugtype:'Tablet',supplier:'SSL',drugstrength:'1.5',unit:'mg',unitprice:'0.60',mimimumqty:3000,manufacturedate:'2020-07-10',expirydate:'2027-11-27'},
      {genericname:'Paracetamol',brandname:'Tylenol',drugtype:'Tablet',supplier:'SSL',drugstrength:'2.5',unit:'mg',unitprice:'5.00',mimimumqty:400,manufacturedate:'2020-03-10',expirydate:'2029-10-27'},
      

    )
     this.dataSource3 = new MatTableDataSource(this.PrescribeDespensaryElement) ;
     this.dataSource3.paginator = this.paginator;
  }

  applyFilter(filterValue:string){
    this.dataSource3.filter = filterValue.trim().toLowerCase();
  }
  removePharmacy(element){
console.log(element)
  }

  SaveMedicine(){

  }
}
