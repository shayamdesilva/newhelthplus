import { Diagnosis } from "./diagnosis";
import { PrescribeDrug } from "./prescribe-drug";
import { RedeemPrescribeDrugDTO } from "./redeem-prescribe-drug-dto";

export class Prescription {
    constructor(
         public id: number,
        public createDate: Date,
        public isDipensaryPrint: boolean,
        public isPharmacyPrint: boolean,
        public prescribeDrugDTO:Array<PrescribeDrug>,
        public redeemPrescribeDrugDTOs:Array<RedeemPrescribeDrugDTO>
        // public diagnosis:Diagnosis
    ){}
}
