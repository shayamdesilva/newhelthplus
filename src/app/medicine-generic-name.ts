export class MedicineGenericName {

    constructor(
        public id: number,
        public userId: number,
        public clinicId:number,
        public createDate: string,
        public genericName: string
    ){}
}
