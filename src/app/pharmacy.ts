export class Pharmacy {
    constructor(
        public id: string,
        public medicine: string,
        public dose: string,
        public frequency: string,
        public day: string,
        public qty: string,
    ){}
}
