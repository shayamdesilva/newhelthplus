import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isTypeOperatorNode } from 'typescript';
import { User } from './user';
import {catchError} from 'rxjs/operators'
import {throwError} from 'rxjs'
import { SearchUser } from './search-user';
import { Diagnosis } from './diagnosis';
import { Login } from './login';
import { Patient } from './patient';
import { Allergy } from './allergy';
import { Investigation } from './investigation';
import { DoctorToClinic } from './doctor-to-clinic';
import { Doctor } from './doctor';
import { MedicineGenericName } from './medicine-generic-name';
import { MedicineBrandSettingDTO } from './medicine-brand-setting-dto';
import { SupplierDTO } from './supplier-dto';
import { DrugTypeDTO } from './drug-type-dto';
import { MedicalBrandNameDTO } from './medical-brand-name-dto';


@Injectable({
  providedIn: 'root'
})
export class EnrollmentService {

  _url_register_patient = 'http://localhost:8080/api/patient/registerpatient';
  _urlsearch_patient = 'http://localhost:8080/api/patient/getpatientsbyname';
  _urlcreatediagnosis = "http://localhost:8080/api/patient/creatediagnosis"
  _doctorlogin = "http://localhost:8080/api/doctor/login_user"
  _userById = "http://localhost:8080/api/doctor/get_doctor_by_doctor_id";
  _registerUser = "http://localhost:8080/api/user/registeruser";
_past_medical_history_by_patient_id = "http://localhost:8080/api/patient/getdiagnosisbypatientid";
_create_allergy = "http://localhost:8080/api/patient/createallergybypatientid";
_get_allergy_by_patient_id = "http://localhost:8080/api/patient/getallergybypatientid";
_update_investigations = "http://localhost:8080/api/patient/updateinvestigationbyid";
_userlogin = "http://localhost:8080/api/clinic/loginuser"
_get_clinic_user_by_id ="http://localhost:8080/api/clinic/getclinicuserbyid"
_get_all_doctors = "http://localhost:8080/api/doctor/get_all_doctor";
_createLinkDoctorClinic = "http://localhost:8080/api/clinic/adddoctortoclinic"
_create_system_user = "http://localhost:8080/api/clinic/adduser"
_register_doctor_account = "http://localhost:8080/api/doctor/adddoctor";
_create_medicine_generic_name = "http://localhost:8080/api/inventory/createmedicinegenericname";
_get_all_generic_name_by_clinic_id = "http://localhost:8080/api/inventory/getallmedicinegenericnamebyclinicid"
_get_all_medicine_name_by_clinic_id = "http://localhost:8080/api/inventory/getAllMedicinebrandsetting"

_delete_medicine_by_id =  "http://localhost:8080/api/inventory/deletelmedicinegenericnamebyclinicid"
_create_medicine_brand_setting = "http://localhost:8080/api/inventory/createMedicinebrandsetting"
_delete_medicine_brand_setting = "http://localhost:8080/api/inventory/deleteMedicinebrandsetting"
_create_supplier = "http://localhost:8080/api/inventory/createSupplier"
_delete_supplier ="http://localhost:8080/api/inventory/deleteSupplier"
_get_all_supplier = "http://localhost:8080/api/inventory/getAllSupplier"
_create_drug_type = "http://localhost:8080/api/inventory/createdrugtype"
_get_all_drug_type ="http://localhost:8080/api/inventory/getalldrugtype"
_delete_drug_type= "http://localhost:8080/api/inventory/deletedrugtype"
_get_all_drug_type_image= "http://localhost:8080/api/inventory/getalldrugtypeImage"
_get_all_medicine_brand_name = "http://localhost:8080/api/inventory/getallmediclebrandname"
_delete_medicine_brand_name = "http://localhost:8080/api/inventory/deletemediclebrandname"
_create_medicine_brand_name ="http://localhost:8080/api/inventory/createmediclebrandname"
_get_prescription_drugs_by_id="http://localhost:8080/api/patient/getdiagnosisbyid"
_get_drug_balance_by_id ="http://localhost:8080/api/report/getdrugbalancebybrandid"

  constructor(private _http: HttpClient) { }

  getPrescriptionDrugsById(diagnosis :Diagnosis ){
    return this._http.post<any>(this._get_prescription_drugs_by_id,diagnosis)
    .pipe(catchError(this.errorHandler))
  }

  getDrugsBalanceById(medicinesetting :MedicalBrandNameDTO ){
    return this._http.post<any>(this._get_drug_balance_by_id,medicinesetting)
    .pipe(catchError(this.errorHandler))
  }

  getAllMedicineBrandName(medicalBrandNameDTO :MedicalBrandNameDTO ){
    return this._http.post<any>(this._get_all_medicine_brand_name,medicalBrandNameDTO)
    .pipe(catchError(this.errorHandler))
  }

  createMedicineBrandName(medicalBrandNameDTO :MedicalBrandNameDTO ){
    return this._http.post<any>(this._create_medicine_brand_name,medicalBrandNameDTO)
    .pipe(catchError(this.errorHandler))
  }

  deleteMedicineBrandName(medicalBrandNameDTO :MedicalBrandNameDTO ){
    return this._http.post<any>(this._delete_medicine_brand_name,medicalBrandNameDTO)
    .pipe(catchError(this.errorHandler))
  }

  getAllDrugTypeImage(drugtypeDTO :DrugTypeDTO ){
    return this._http.post<any>(this._get_all_drug_type_image,drugtypeDTO)
    .pipe(catchError(this.errorHandler))
  }

  createDrugType(drugtypeDTO :DrugTypeDTO ){
    return this._http.post<any>(this._create_drug_type,drugtypeDTO)
    .pipe(catchError(this.errorHandler))
  }

  deleteDrugType(drugtypeDTO :DrugTypeDTO ){
    return this._http.post<any>(this._delete_drug_type,drugtypeDTO)
    .pipe(catchError(this.errorHandler))
  }

  getAllDrugType(drugtypeDTO :DrugTypeDTO ){
    return this._http.post<any>(this._get_all_drug_type,drugtypeDTO)
    .pipe(catchError(this.errorHandler))
  }

  createSupplier(supplierDTO :SupplierDTO ){
    return this._http.post<any>(this._create_supplier,supplierDTO)
    .pipe(catchError(this.errorHandler))
  }

  deleteSupplier(supplierDTO :SupplierDTO ){
    return this._http.post<any>(this._delete_supplier,supplierDTO)
    .pipe(catchError(this.errorHandler))
  }

  getAllSupplier(supplierDTO :SupplierDTO ){
    return this._http.post<any>(this._get_all_supplier,supplierDTO)
    .pipe(catchError(this.errorHandler))
  }


  deleteMedicineBrasndSetting(medicinesetting :MedicineBrandSettingDTO ){
    return this._http.post<any>(this._delete_medicine_brand_setting,medicinesetting)
    .pipe(catchError(this.errorHandler))
  }

  enroll(patient: Patient){
    return this._http.post<any>(this._url_register_patient,patient)
    .pipe(catchError(this.errorHandler))
  }

  searchEnrollUser(searchUser: SearchUser){
    return this._http.post<any>(this._urlsearch_patient,searchUser)
    .pipe(catchError(this.errorHandler))
  }

  errorHandler(error:HttpErrorResponse){
    return throwError(error);
  }

  enrollCreateDiagnosis(diagnosis1: Diagnosis){
    return this._http.post<any>(this._urlcreatediagnosis,diagnosis1)
    .pipe(catchError(this.errorHandler))
  }

  userLogin(doctor: Doctor){
    return this._http.post<any>(this._userlogin,doctor)
    .pipe(catchError(this.errorHandler))
  }

  doctorLogin(doctor: Doctor){
    return this._http.post<any>(this._doctorlogin,doctor)
    .pipe(catchError(this.errorHandler))
  }


  enrollUserById(user: User){
    return this._http.post<any>(this._userById,user)
    .pipe(catchError(this.errorHandler))
  }

  registerUser(user: User){
    return this._http.post<any>(this._registerUser,user)
    .pipe(catchError(this.errorHandler))
  }

  enrollGetDiagnosisPatientsDetails(patient: Patient){
    return this._http.post<any>(this._past_medical_history_by_patient_id,patient)
    .pipe(catchError(this.errorHandler))
  }

  enrollCreateAllergy(allergy: Allergy){
    return this._http.post<any>(this._create_allergy,allergy)
    .pipe(catchError(this.errorHandler))
  }

  enrollGetAllergyByPatientId(patient: Patient){
    return this._http.post<any>(this._create_allergy,patient)
    .pipe(catchError(this.errorHandler))
  }

  enrollUpdateInvestigationById(patient: Investigation){
    return this._http.post<any>(this._update_investigations,patient)
    .pipe(catchError(this.errorHandler))
  }

  getClinicUserById(doctor: Doctor){
    return this._http.post<any>(this._get_clinic_user_by_id,doctor)
    .pipe(catchError(this.errorHandler))
  }

  getAllDoctors(){
    return this._http.post<any>(this._get_all_doctors,null)
    .pipe(catchError(this.errorHandler))
  }

  ctreateLinkDoctorToClinic(doc :DoctorToClinic ){
    return this._http.post<any>(this._createLinkDoctorClinic,doc)
    .pipe(catchError(this.errorHandler))
  }

  createSystemUser(user :User ){
    return this._http.post<any>(this._create_system_user,user)
    .pipe(catchError(this.errorHandler))
  }

  registerDoctorAccount(doctor :Doctor ){
    return this._http.post<any>(this._register_doctor_account,doctor)
    .pipe(catchError(this.errorHandler))
  }

  createMedicineGenericName(medicineGenericName :MedicineGenericName ){
    return this._http.post<any>(this._create_medicine_generic_name,medicineGenericName)
    .pipe(catchError(this.errorHandler))
  }

  getAllGenericNameByClinicId(medicineGenericName :MedicineGenericName ){
    return this._http.post<any>(this._get_all_generic_name_by_clinic_id,medicineGenericName)
    .pipe(catchError(this.errorHandler))
  }

  getAllMedicineNameByClinicId(medicineGenericName :MedicineGenericName ){
    return this._http.post<any>(this._get_all_medicine_name_by_clinic_id,medicineGenericName)
    .pipe(catchError(this.errorHandler))
  }

  deleteMedicineNameById(medicineGenericName :MedicineGenericName ){
    return this._http.post<any>(this._delete_medicine_by_id,medicineGenericName)
    .pipe(catchError(this.errorHandler))
  }

  createMedicineBrasndSetting(medicinesetting :MedicineBrandSettingDTO ){
    return this._http.post<any>(this._create_medicine_brand_setting,medicinesetting)
    .pipe(catchError(this.errorHandler))
  }
}
