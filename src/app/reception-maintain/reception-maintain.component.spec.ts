import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceptionMaintainComponent } from './reception-maintain.component';

describe('ReceptionMaintainComponent', () => {
  let component: ReceptionMaintainComponent;
  let fixture: ComponentFixture<ReceptionMaintainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReceptionMaintainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptionMaintainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
