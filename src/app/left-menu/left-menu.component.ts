import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import {MatMenuModule, MatMenuTrigger} from '@angular/material/menu';
import * as $ from 'jquery';
import {MatSidenavModule} from '@angular/material/sidenav';
import { Config, Menu } from '../accordion/types';
@Component({
  selector: 'app-left-menu',
  template: `
  <h1>Accordion Menu</h1>
  <accordion 
    [options]="options" 
    [menus]="menus">
  </accordion>
`,
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css']
})
export class LeftMenuComponent  {
  options: Config = { multi: false };
 
  config: Config;
  ngOnInit() {
    this.config = this.mergeConfig(this.options);
  }

  mergeConfig(options: Config) {
    const config = {
      // selector: '#accordion',
      multi: true
    };

    return { ...config, ...options };
  }

  toggle(index: number,menuName,submenuName) {
    console.log('Index '+menuName)
    localStorage.setItem('currentMenuItemName',menuName+'/'+submenuName);
    if (!this.config.multi) {
      this.menus.filter(
        (menu, i) => i !== index && menu.active
      ).forEach(menu => menu.active = !menu.active);
    }

    this.menus[index].active = !this.menus[index].active;
  }

  menus: Menu[] = [
    { 
      name: 'Dashboard' , 
      menuUrl: '/dashboard',
      iconClass: '0',
      active: true,
      submenu: []
    },
    { 
      name: 'Patient',
      menuUrl: 'View',
      iconClass: '1',
      active: false,
      submenu: [
        // { name: 'Patient Registration', url: '/patientregistration' },
        { name: 'Manintenance', url: '/maintenance' },
        { name: 'Reception Manintenance', url: '/receptionmaintenance' },
        { name: 'Prescription Check out', url: '/prescriptioncheckout' },
      ]
    },
    { 
      name: 'Inventory',
      menuUrl: 'View',
      iconClass: '1',
      active: false,
      submenu: [
        { name: 'Medicine', url: '/addmedicine' },
        { name: 'Generic Name', url: '/addgenericname' },
        { name: 'Medical Brand Name', url: '/medicalbrandname' },
        { name: 'Brand Setting', url: '/brandsetting' },
        { name: 'Supplier', url: '/supplier' },
        { name: 'Drug Type', url: '/drugtype' },
      ]
    },
    { 
      name: 'Reports',
      menuUrl: 'Reports',
      iconClass: '1',
      active: false,
      submenu: [
        { name: 'Stock', url: '/stock' },
      ]
    },
    { 
      name: 'Settings',
      menuUrl: 'Settings',
      iconClass: '1',
      active: false,
      submenu: [
        { name: 'Administration', url: '/Setting/Administration' },
      //   { name: 'User Profile', url: '#' }
       ]
    },
    { 
      name: 'Admin Panel',
      menuUrl: 'Settings',
      iconClass: '1',
      active: false,
      submenu: [
        { name: 'Search Doctor', url: '/searchdoctor' },
        { name: 'Create System user', url: '/createsystemuser' },
      //   { name: 'User Profile', url: '#' }
       ]
    },
    { 
      name: 'My Profile',
      menuUrl: '/myprofile',
      iconClass: '1',
      active: false,
      submenu: [
         { name: 'Register Doctor Profile ', url: '/registerdoctorprofile' },
         { name: 'Doctor Login ', url: '/doctorlogin' }
      //   { name: 'User Profile', url: '#' }
       ]
    }
  ];



  constructor(private router:Router) {

   }



 

  addUser(){
    localStorage.setItem('MenuItemName','addUser');
    this.router.navigate(['/user/register']);


  }

}
