import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import * as alertify from 'alertifyjs'
import { data, trim } from 'jquery';
import { element } from 'protractor';
import { EnrollmentService } from '../enrollment.service';
import { User } from '../user';
import {MatSnackBar} from '@angular/material/snack-bar';
import { SearchUser } from '../search-user';
import { ManintenanceComponent } from '../manintenance/manintenance.component';
import { Patient } from '../patient';
import { ClinicUser } from '../clinic-user';
import { Doctor } from '../doctor';
import { PrescriptionCheckOutComponent } from '../prescription-check-out/prescription-check-out.component';


export interface PeriodicElement {
  lname: string;
  fname: number;
  dob: number;
  nic: string;
  address: string;
  phone: string;
}

// const ELEMENT_DATA: PeriodicElement[] = [
//   {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
//   {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
//   {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
//   {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
//   {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
// ];



@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {
  userModelTemp;
  doctorModel = new Doctor(null,'','','','','','','','','','','','','',null,null,false,null,false,null);
  userDto = new User(null,'',null,null,'','',null,'',false,'','');
  clinicUserObject;
  patientModel = new Patient(null,'','','','','','','','','','',false,'',false,'',false,null,null,'','',null,'','');
  patientList:any[];
  searchUserModel = new SearchUser('','',null,'','','');
  submitted = false;
  errorMsg = '';
  panelOpenState = false;
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  displayedColumns: string[] = ['id','lname', 'fname', 'dob', 'nic',"address","phone"];
  dataSource;  
  ELEMENT_DATA;
  minDate = new Date();
  maxDate = new Date(2019-3-10)
  displayTable = false;
  PeriodicElement:any=[];
  PeriodicElement1:any=[];
  allPatients:any=[];
  mValue:String;
  selectGender: string;
  gender: string[] = ['Male', 'Female'];
  clinicUsers:any=[];

  

  constructor(private modalService: NgbModal,private http:HttpClient,
    private EnrollmentService:EnrollmentService,private _snackBar: MatSnackBar, private maintaince:ManintenanceComponent) { 
    this.dataSource = this.ELEMENT_DATA;
    // this.PeriodicElement = [
    //   {id:'1', fname: 'Pubudu ', lname: 'De Silva',dob:'1985/08/17',nic:'646465353V', address: 'No 100 St. Marys Road Jaela', phone: '8777666666'},
    //   {id:'2',fname: 'Thushani ', lname: 'Fernando',dob:'1985/05/17',nic:'646434353V', address: 'No 43 St. Marys Road Jaela', phone: '787777666666'},
    //   {id:'3',fname: 'Ryal ', lname: 'Fernando',dob:'2020/05/17',nic:'646434353V', address: 'No 677 St. Marys Road Jaela', phone: '787777666666'},
    //   {id:'4',fname: 'Shelton ', lname: 'Fernando',dob:'1985/05/17',nic:'646487353V', address: 'No 344 St. Marys Road Jaela', phone: '787777666666'},
    //   {id:'5',fname: 'Ranjith ', lname: 'Fernando',dob:'1985/05/17',nic:'998434353V', address: 'No 43 St. Marys Road Jaela', phone: '787777666666'},
    // ];
    // this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
    // this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
    // this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.allPatients = this.getAllPatients();
    console.log('All Patient = '+this.allPatients)
    this.getLoggingUserById(localStorage.getItem("tokenid"));

  }

  

  onSubmit(){
    this.submitted = true;
    console.log('SLMC '+this.userDto['slmcNumber']);
    console.log('CLINIC '+this.userDto['clinicId']);
    // console.log('User name => '+this.doctorModel.userName)
    this.patientModel.doctorId = localStorage.getItem("tokenid");
    if(this.userDto.isDoctor){
      this.patientModel.doctorSlmc = this.userDto['slmcNumber'];
    }else{
      this.patientModel.doctorSlmc = this.patientModel.slmc;
    }

    this.patientModel.clinicId = this.userDto['clinicId'];
    console.log('Is Doc '+this.userDto.isDoctor)
    console.log('Slmc '+this.patientModel.slmc);

    this.EnrollmentService.enroll(this.patientModel)
      .subscribe(
      data => {
        
    console.log('Status : '+data['status']);
        if(data['status'] == 1){
         
          this._snackBar.open("Saved", "Patient Saved", {
            duration: 2000,
          });
        }
        if(data['status'] == 0){
          this._snackBar.open("Patient Not Saved", "Not saved" ,{
            duration: 2000,
          });
        }
      }
      ,
      error => this.errorMsg = error.statusText
    );

    
  }

  Clean(){
    console.log('Clear...')
    // this.userModel = new User(null,'','',null,'','','','','','','');
    this.patientModel = new Patient(0,'','',null,'','','','','','','',false,'',false,'',false,null,null,'','',null,'','');
  }

  EditPatient(element){
    console.log(this.userModelTemp.dateOfBirth);
    this.patientModel = this.userModelTemp;
    this.patientModel.dateOfBirth = new Date(this.userModelTemp.dateOfBirth).toLocaleDateString('en-CA'),
   console.log('Dt : '+this.doctorModel.dateOfBirth);
    

  }


  

  masterToggle($event,element) {
    console.log(element);
    this.userModelTemp = element;
    // if ($event.checked) {
    //   this.onCompleteRow(this.dataSource);
    // }
    // this.isAllSelected($event) ?
    //   this.selection.clear() :
    // this.dataSource.data.forEach(row => this.selection.select(row));
  }

  

  onSearch(){
    console.log("Searching user...");
    console.log('User Model '+this.doctorModel);
  }

  applyFilter(filterValue:string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openNewPatientPopUp(content) {
    console.log('Button Click');
    this.modalService.open(content, { size: 'xl' });
  }

  searchPatient(){
    this.displayTable = true;
    this.searchUserModel.doctorId = localStorage.getItem("tokenid");
    this.searchUserModel.doctorSlmc = this.userDto['slmcNumber']
    this.searchUserModel.clinicId = localStorage.getItem('clinicId');

    this.EnrollmentService.searchEnrollUser(this.searchUserModel)
    .subscribe(
      data => {
        
        this.PeriodicElement = data['patientDTO'];
            this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
    this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
      }
    )
  }



  SavePatient(){
    var firstName = ((document.getElementById("txtFirstName") as HTMLInputElement).value);
    var lastName = ((document.getElementById("txtLastName") as HTMLInputElement).value);
    var dateOfBirth = ((document.getElementById("txtDateOfBirth") as HTMLInputElement).value);
    var nic = ((document.getElementById("txtNIC") as HTMLInputElement).value);
    //var gender = ((document.getElementById("radioGender") as HTMLInputElement).checked);
    var address = ((document.getElementById("txtAddress") as HTMLInputElement).value);
    var phoneNumber = ((document.getElementById("txtPhoneNumber") as HTMLInputElement).value);
    var emailAddress = ((document.getElementById("txtEmailAddress") as HTMLInputElement).value);
    var emergencyContactName = ((document.getElementById("txtEmergencyContactName") as HTMLInputElement).value);
    var emergencyContactNumber = ((document.getElementById("txtEmergencyContactNumber") as HTMLInputElement).value);
   // var x =((document.getElementById("radioMale") as HTMLInputElement).checked);

    console.log('DOB...'+dateOfBirth);
    //console.log('gender...'+gender);
    //console.log('male...'+x);
    console.log('mValue...'+this.selectGender);
  }



  getAllPatients():Observable<String[]>{
    return this.http.get<String[]>('http://localhost:8080/api/users/getallpatients');
    }

    nextPatient(){
      console.log('Next Patient' +this.userModelTemp.firstName)
      this.maintaince.setPatientModel(this.userModelTemp);
      // this.prescriptionCheckOut.setPatientModel(this.userModelTemp);

    }

    public getSystemDoctors(id){
      this.doctorModel.id = id;
      this.EnrollmentService.getClinicUserById(this.doctorModel)
      .subscribe(
        data => {
          // console.log('Maintaince Data '+data['userDTos'][0]['firstName'])
          this.doctorModel = null;
          this.userDto = data['userDTO'];
          this.userDto.isDoctor = data['userDTO']['doctor'];
          this.clinicUserObject = data;
          this.clinicUsers = data['clinicDoctors'];
          console.log('Size => '+this.userDto.isDoctor)
            
        }
      )
    }

    public getLoggingUserById(id){
      this.doctorModel.id = id;

      this.EnrollmentService.getClinicUserById(this.doctorModel)
      .subscribe(
        data => {
          // console.log('Maintaince Data '+data['userDTos'][0]['firstName'])
          this.doctorModel = null;
          this.userDto = data['userDTO'];
          this.userDto.isDoctor = data['userDTO']['doctor'];
          this.clinicUserObject = data;
          this.clinicUsers = data['clinicDoctors'];
          console.log('Size => '+this.clinicUsers.length)
            
        }
      )
    }
 

}
