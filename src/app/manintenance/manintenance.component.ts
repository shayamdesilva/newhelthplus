import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import * as alertify from 'alertifyjs'
import { trim } from 'jquery';
import { element } from 'protractor';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Medicine } from '../Medicine';
import { Diagnosis } from '../diagnosis';
import { Prescription } from '../prescription';
import { PrescribeDrug } from '../prescribe-drug';
import { EnrollmentService } from '../enrollment.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { createFalse } from 'typescript';
import { User } from '../user';
import { Investigation } from '../investigation';
import { Fee } from '../fee';
import { PrescriptionDTO } from '../prescription-dto';
import { DateRange } from '@angular/material/datepicker';
import { Print } from '../print';
import { Patient } from '../patient';
import { PatientHistory } from '../patient-history';
import { Allergy } from '../allergy';
import { ClinicUser } from '../clinic-user';
import { Doctor } from '../doctor';
import { MedicineGenericName } from '../medicine-generic-name';
import { Clinic } from '../clinic';

export interface PeriodicElement {
  name: string;
  position: string;
}

export interface PeriodicElement1 {
  name: string;
  position: string;
}

export interface PeriodicElement2 {
  name: string;
  position: string;
}

export interface PrescribeDespensaryElement {
  medicine: string;
  nooftime: string;
  day: string;
  qty: string;
  stockid:string;
  stockqty:number;
}

export interface PrescribePharmacyElement {
  medicine: string;
  nooftime: string;
  day: string;
  qty: string;
  stockid:string;
  stockqty:number;
}

@Component({
  selector: 'app-manintenance',
  templateUrl: './manintenance.component.html',
  styleUrls: ['./manintenance.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],

})
export class ManintenanceComponent implements OnInit {
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  dispensaryDrugList: Array<PrescribeDrug> = [];
  PharmacyDrugList: Array<PrescribeDrug> = [];
  commonDrugList: Array<PrescribeDrug> = [];
  dispensaryElement:any=[];
  pharmacyElement:any=[];
  count = 0;
  investigationId;
  printButtonDisabled:boolean;
  pdfResult:string;
  showLoadingIndicator = true;
  medicineGenericModel = new MedicineGenericName(null,null,null,'','');
  clinicDto = new Clinic(null,null,'','','',false)
  doctorModel = new Doctor(null,'','','','','','','','','','','','','',null,null,false,null,false,null);
  userDto = new User(null,'',null,null,'','',null,'',false,'','');
  clinicUserObject;
  patientModel = new Patient(null,'','','','','','','','','','',false,'',false,'',false,null,null,'','','','','');
  allergy = new Allergy(null,null,null,'')
  edicineModel = new Medicine(0,'','','','','');
  diagnosisModel = new Diagnosis(null,null,0,'','','',null,null,null,'');
  investigation = new Investigation(null,'','','','');
  investigationEdit = new Investigation(null,'','','','');
  fee = new Fee(null,null,null,'',false);
  prescriptionModel = new Prescription(null,null,false,false,null,null);
  prescribeDrugModel = new PrescribeDrug(null,'','',0,0,0,0,'',null);
  today:Date;
  pdfData:any=[];
  brandSetting:any=[];

  // diagnosisModel1: Diagnosis;
  prescriptionModel1;
  prescribeDrugModel1;
  displayedColumns: string[] = ['Date', 'Presentedcomplain' ,'Examination', 'Precription'];
  displayedColumnsAllergies: string[] = ['position', 'name'];
  displayedColumnsTests: string[] = ['id','position', 'name','result'];
  displayedColumnsDispancery: string[] = ['stockid','medicine','dose', 'nooftime','day','qty','stockqty'];
  displayedColumnsPharmacy: string[] = ['stockid','medicine','dose', 'nooftime','day','qty'];
  dataSource;  
  ELEMENT_DATA;
  dataSource1;
  dataSource2;
  dataSource3;
  dataSourceDiapensary;  
  dataSourcePhamacy;  
  
  ELEMENT_DATA1;
  ELEMENT_DATA3;
  ELEMENT_DATA4;
  ELEMENT_DATA5;
  PeriodicElement:any=[];
  PeriodicElement1:any=[];
  PeriodicElement2:any=[];
  PrescribeDespensaryElement:any[];
  PrescribePharmacyElement:any[];
  constructor(private modalService: NgbModal,private http:HttpClient,private EnrollmentService:EnrollmentService
    ,private _snackBar: MatSnackBar) {
    this.dataSource = this.ELEMENT_DATA;
    this.dataSource1 = this.ELEMENT_DATA1;
    this.dataSource2 = this.ELEMENT_DATA3;
    this.dataSource3 = this.ELEMENT_DATA4;
    this.PrescribePharmacyElement = this.ELEMENT_DATA4;
   }

  ngOnInit(): void {
    this.printButtonDisabled = true;
    this.today = new Date();
    console.log("Name "+localStorage.getItem("token"))
    console.log("User Object "+localStorage.getItem("token"))
    this.getLoggingUserById(localStorage.getItem("tokenid"));
  }

  applyFilter(filterValue:string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  SaveDiagnosis(){
    console.log('Diagnos')
  }


  adddispensary(){
    
    // console.log(this.medicineModel)
    
  }

  onSubmit(){
    console.log('Submit');
  }

  addMedicine(event){
 this.count = this.count + 1;
 console.log(event);
 console.log('Prescribe Drug Model ===>  '+this.prescribeDrugModel)
 console.log(this.brandSetting)

    if(event === 'Dispensary'){
      let prscDrug = new PrescribeDrug(null,'','',0,0,0,0,'',null);
      prscDrug.counterId = this.count;
      // prscDrug.id = this.prescribeDrugModel.id;
      prscDrug.numberOfDay = this.prescribeDrugModel.numberOfDay;
      prscDrug.dose = this.prescribeDrugModel.dose;
      prscDrug.frequency = this.prescribeDrugModel.frequency;
      prscDrug.drug = this.prescribeDrugModel.drug;
      prscDrug.quantity = this.prescribeDrugModel.quantity;
      prscDrug.catagory = "D";
      this.dispensaryDrugList.push(prscDrug)
      this.commonDrugList.push(prscDrug);
    this.dataSourceDiapensary = new MatTableDataSource(this.dispensaryDrugList) ;
    }else{
      let prscDrug = new PrescribeDrug(null,'','',0,0,0,0,'',null);
      prscDrug.counterId = this.count;
      prscDrug.numberOfDay = this.prescribeDrugModel.numberOfDay;
      prscDrug.dose = this.prescribeDrugModel.dose;
      prscDrug.frequency = this.prescribeDrugModel.frequency;
      prscDrug.drug = this.prescribeDrugModel.drug;
      prscDrug.quantity = this.prescribeDrugModel.quantity
      prscDrug.catagory = "P"
      this.PharmacyDrugList.push(prscDrug)
      this.commonDrugList.push(prscDrug);
      this.dataSourcePhamacy = new MatTableDataSource(this.PharmacyDrugList) ;
    }
    


    console.log(this.PharmacyDrugList);

  }

  loadingOn(){
    this.showLoadingIndicator = true;
  }

  loadingOff(){
    this.showLoadingIndicator = false;
  }

  addpharmacy(){
    console.log(this.dispensaryElement)

  }

   arrayRemove(arr, value) { 
    
    return arr.filter(function(ele){ 
        return ele != value; 
    });
}

  removePharmacy(element){
    console.log('remove pharmacy...'+element['id'])
    console.log(this.dispensaryDrugList)

    for(var x = 0; x<this.PharmacyDrugList.length;x++){
      console.log(this.PharmacyDrugList[x]['id'])
      if(this.PharmacyDrugList[x]['counterId'] ===element['id'] ){
        this.PharmacyDrugList.splice(x,1)
        console.log('remove Dispensary...'+element['id'])
        this.dataSourcePhamacy = new MatTableDataSource(this.PharmacyDrugList) ;
      }
    }
  }

  removeDispansary(element){//masterToggle($event,element)
    
    this.arrayRemove(this.dispensaryDrugList,element['id']);
    for(var x = 0; x<this.dispensaryDrugList.length;x++){
      console.log(this.dispensaryDrugList[x]['id'])
      if(this.dispensaryDrugList[x]['id'] ===element['id'] ){
        this.dispensaryDrugList.splice(x,1)
        console.log('remove Dispensary...'+element['id'])
      }
    }
  }

  print(type){
    console.log(type)
    if(type === 'Dispensary'){
      this.pdfData = this.dispensaryDrugList;
    }else{
      this.pdfData = this.PharmacyDrugList;
    }
    this.loadPrintDate("My Print Dock....")
  }
  
  createDiagnosis(){

    console.log('Drug Length >>> '+this.commonDrugList.length);
    console.log('Investigation ==> '+this.investigation.investgationDescription)
    console.log('User Id in create Disg : '+this.doctorModel.id);
    var currentPrescriptionModel = new Prescription(null,new Date(),false,false,this.commonDrugList,null);

    var currentDiagnosisModel = new Diagnosis(null,new Date(),0,'','','',null,null,null,'');

    this.fee.patientId = this.doctorModel.id;
    this.diagnosisModel.prescriptionDTO = currentPrescriptionModel;
    this.diagnosisModel.feeDTO = this.fee;
    this.diagnosisModel.investigationDTO = this.investigation;
    this.diagnosisModel.patientId = this.doctorModel.id;
    this.diagnosisModel.doctorId = localStorage.getItem("tokenid");
    this.diagnosisModel.doctorSlmc = localStorage.getItem("slmc");
    currentDiagnosisModel = this.diagnosisModel;

    this.EnrollmentService.enrollCreateDiagnosis(currentDiagnosisModel)
    .subscribe(
      data => {
        this.printButtonDisabled = false;
          this._snackBar.open("Saved", "Patient Saved", {
            duration: 2000,
          });

          this.loadPastMedicalHistory(this.doctorModel);
          
      }
    )
  }

  public getLoggingUserById(id){
    this.doctorModel.id = id;

        this.EnrollmentService.getClinicUserById(this.doctorModel)
    .subscribe(
      data => {
        // console.log('Maintaince Data '+data['userDTos'][0]['firstName'])
        this.doctorModel = null;
        this.userDto = data['userDTO'];
        this.clinicDto = data['clinicDTO'];
        this.clinicUserObject = data;
        this.loadMedicineBrandName();
          
      }
    )


  }

  loadMedicineBrandName(){//Medicine Dropdown
    this.medicineGenericModel.clinicId = this.clinicDto.id
    this.medicineGenericModel.userId = this.userDto.id
    this.EnrollmentService.getAllMedicineNameByClinicId(this.medicineGenericModel)
    .subscribe(
      data => {
        this.brandSetting = data.medicineBrandSettingDTOs
      }
    )

  }

  public setUserModel(model){
    console.log(model);
    this.doctorModel = model;
    console.log('User Id : '+this.doctorModel.id);
    console.log('User Id : '+this.doctorModel.firstName);
    console.log('User Id : '+this.doctorModel.lastName);
  }

  public setPatientModel(model){
    console.log(model);
    this.doctorModel = model;
    console.log('User Id : '+this.doctorModel.id);
    console.log('User Id : '+this.doctorModel.firstName);
    console.log('User Id : '+this.doctorModel.lastName);
    this.loadPastMedicalHistory(model)
    

  }

  masterToggle(event,element) {
    console.log('Element :'+this.dataSource3['investgationResult']);
    console.log('Event '+element['id'])
    this.investigationId = element['id'];
    this.investigationEdit = element;
    console.log('Result '+this.investigationEdit.investgationResult)
    // this.userModelTemp = element;
    // if ($event.checked) {
    //   this.onCompleteRow(this.dataSource);
    // }
    // this.isAllSelected($event) ?
    //   this.selection.clear() :
    // this.dataSource.data.forEach(row => this.selection.select(row));
  }

  createInvestigation( ){
    // console.log('Result 0==> '+element)
    console.log('Result 1==> '+this.investigationEdit.investgationResult)
    console.log('Result 2==> '+this.PeriodicElement2[0]['investgationResult'])
    console.log('dataSource3 '+((document.getElementById(this.investigationId) as HTMLInputElement).value))
    this.investigationEdit.investgationResult = ((document.getElementById(this.investigationId) as HTMLInputElement).value);
    this.diagnosisModel.investigationDTO = this.PeriodicElement2[0];

    this.EnrollmentService.enrollUpdateInvestigationById(this.investigationEdit)
    .subscribe(
      data => {
        if(data.diagnosisDTOs.length>0){
          for(var x = 0;x<data.diagnosisDTOs.length;x++){
            console.log(data.diagnosisDTOs[x]['investigationDTO']['investgationDescription']);
          }
        }
          
      }
    )
  }

  loadPastMedicalHistory(model){
    this.patientModel = model;
    this.PeriodicElement.length = 0;

    this.EnrollmentService.enrollGetDiagnosisPatientsDetails(this.patientModel)
    .subscribe(
      data => {
        // console.log('PastMedicalHistory=====>>>> '+data.diagnosisDTOs[0]['prescriptionDTO']['id'])
        // console.log('Length=====>>>> '+data.diagnosisDTOs.length)
        
        if(data.diagnosisDTOs.length>0){
          for(var x = 0;x<data.diagnosisDTOs.length;x++){
            var dispensary = ""; 
            var pharmacy = "";
            var history = new PatientHistory(null,null,'','','','','');
            history.Date = new Date(data.diagnosisDTOs[x]['createDate']).toLocaleDateString('en-CA'),
            history.Presentedcomplain = data.diagnosisDTOs[x]['presentingComplain']
            history.Examination = data.diagnosisDTOs[x]['examination']
           history.Precription = "^ click here";
            // console.log('Drug length '+data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'].length)
           
            for(var j = 0; j<data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'].length;j++){
               
              if(data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['catagory'] === 'D'){
                  dispensary = dispensary +'    Drug: '+ data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['drug'];
                  dispensary = dispensary + '  Dose: '+ data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['dose'];
                  dispensary = dispensary + '  Frequency: ' + data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['frequency'];
                  dispensary = dispensary + '  No of Days: ' + data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['numberOfDay'];
                  dispensary = dispensary + '  Qty: ' + data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['quantity']+ " --- ";
                }
                if(data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['catagory'] === 'P'){
                  pharmacy = pharmacy +'    Drug: '+ data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['drug'];
                  pharmacy = pharmacy + '  Dose: '+ data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['dose'];
                  pharmacy = pharmacy + '  Frequency: ' + data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['frequency'];
                  pharmacy = pharmacy + '  No of Days: ' + data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['numberOfDay'];
                  pharmacy = pharmacy + '  Qty: ' + data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['quantity']+ " --- ";
                }
                // dispensaryArr.push(dispensary);
                console.log('Catagory is ===>>> '+data.diagnosisDTOs[x]['prescriptionDTO']['prescribeDrugDTO'][j]['catagory']);
            }
            history.description = dispensary;
            history.description1 = pharmacy;
  
            this.PeriodicElement.push(history);
            this.PeriodicElement2.push(data.diagnosisDTOs[x]['investigationDTO']);
          }

       


        }
        this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
    this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

    this.dataSource3 = new MatTableDataSource(this.PeriodicElement2) ;
    this.dataSource3 = new MatTableDataSource(this.PeriodicElement2) ;
    this.dataSource3.sort = this.sort;
    this.dataSource3.paginator = this.paginator;

    if(data.allergyDTOs.length >0){
      this.PeriodicElement1 = data.allergyDTOs;
      this.dataSource1 = new MatTableDataSource(this.PeriodicElement1) ;
      this.dataSource1 = new MatTableDataSource(this.PeriodicElement1) ;
      this.dataSource1.sort = this.sort;
      this.dataSource1.paginator = this.paginator;
    }

      }
    )

  }

  SaveAllergy(){
    this.allergy.patientId = this.doctorModel.id



    this.EnrollmentService.enrollCreateAllergy(this.allergy)
    .subscribe(
      data => {
        console.log(this.doctorModel.id+' '+this.allergy.allergyDescription + " "+data.status)
        if(data.status == 1){
          this._snackBar.open("Saved", "Allergy Saved", {
            duration: 2000,
          });
          this.PeriodicElement1 = data.allergyDTOs;
          this.dataSource1 = new MatTableDataSource(this.PeriodicElement1) ;
          this.dataSource1 = new MatTableDataSource(this.PeriodicElement1) ;
          this.dataSource1.sort = this.sort;
          this.dataSource1.paginator = this.paginator;
     
        }else{
          this._snackBar.open("Save Faill", "Allergy not Saved", {
            duration: 2000,
          });
        }
        
          
      }
    )

  }



  loadPrintDate(data){

    this.pdfResult = '<div class="container">';

    this.pdfResult = this.pdfResult + '<div class="row" >'+
      '<table>'+
      '<tbody>'+
      ' <tr>'+
      '<td></td>'+
      '<td > '+ 'Testnig' +' </td> '+
      '<td  >Test</td> '+
      '</tr>'+
      '</tbody>'+
      '</table>'+
      '</div>'+
      '</div>';

    console.log("Pinnting Data..... "+data);
    var innerContents = document.getElementById('print-section').innerHTML;
    var popupWinindow = window.open('', '', 'width=1000,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWinindow.document.open();
    popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + 
    
    innerContents
    
    + '</html>');
    popupWinindow.document.close();

    console.log("Enf PDF");


  }




}
