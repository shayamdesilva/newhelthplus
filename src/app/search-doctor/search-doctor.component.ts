import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DoctorToClinic } from '../doctor-to-clinic';
import { EnrollmentService } from '../enrollment.service';

@Component({
  selector: 'app-search-doctor',
  templateUrl: './search-doctor.component.html',
  styleUrls: ['./search-doctor.component.css']
})
export class SearchDoctorComponent implements OnInit {
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  showLoadingIndicator = true;
  dataSource3;
  ELEMENT_DATA4;
  displayedColumnsTests: string[] = ['id','First Name', 'Last Name','DOB','Email','slmcNumber','isEmailVerify'];
  doctor_clinic = new  DoctorToClinic(null,null,'','','','','',null,false);
  PeriodicElement2:any=[];
  constructor(private EnrollmentService:EnrollmentService,private _snackBar: MatSnackBar) {
    this.dataSource3 = this.ELEMENT_DATA4;
   }

  ngOnInit(): void {
  }

  SearchDoctor(){
    this.EnrollmentService.getAllDoctors()
    .subscribe(
      data => {

        this.dataSource3 = new MatTableDataSource(data.userDTos) ;
        this.dataSource3 = new MatTableDataSource(data.userDTos) ;
        this.dataSource3.sort = this.sort;
        this.dataSource3.paginator = this.paginator;
        console.log(data.userDTos[0]['id'])
          this._snackBar.open("Saved", "Patient Saved", {
            duration: 2000,
          });

          
      }
    )


  }

  masterToggle(event,element) {
    console.log('Element :'+this.dataSource3['investgationResult']);
    console.log('Event '+element['id'])
    this.doctor_clinic.isEmailVerify= false;
    this.doctor_clinic.clinicId = localStorage.getItem('clinicId');
    this.doctor_clinic.doctorId = element['id'];
    this.doctor_clinic.email = element['email']
    this.doctor_clinic.slmc = element['slmcNumber']

    // this.EnrollmentService.ctreateLinkDoctorToClinic(this.doctor_clinic)
    // .subscribe(
    //   data => {
    //     if(data.status == 1){
    //       this._snackBar.open("Saved", "Doctor Assign to Clinic", {
    //         duration: 2000,
    //       });
    //     }else{
    //       if(data.status == 1){
    //         this._snackBar.open("Saved Faill", "Doctor not assign to clinic", {
    //           duration: 2000,
    //         });
    //     }
          
    //   }
    // }
    // )
      
  }

  SendComformation(){

    console.log('Element :'+this.dataSource3['investgationResult']);
    console.log('Doc Id '+this.doctor_clinic.doctorId)
    console.log('clinicId '+this.doctor_clinic.slmc)
 
        this.EnrollmentService.ctreateLinkDoctorToClinic(this.doctor_clinic)
    .subscribe(
      data => {
        if(data.status == 1){
          this._snackBar.open("Saved", "Doctor Assign to Clinic", {
            duration: 2000,
          });
        }else{
         
            this._snackBar.open("Saved Faill", "Doctor not assign to clinic", {
              duration: 2000,
            });
        
          
      }
    }
    )

  }

  applyFilter(filterValue:string){
    // this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
