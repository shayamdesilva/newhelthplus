export type Menu = {
    name: string, 
    menuUrl: string,
    iconClass: string, 
    active: boolean,
    submenu: { name: string, url: string }[]
  }