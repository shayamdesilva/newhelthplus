import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterDoctorProfileComponent } from './register-doctor-profile.component';

describe('RegisterDoctorProfileComponent', () => {
  let component: RegisterDoctorProfileComponent;
  let fixture: ComponentFixture<RegisterDoctorProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterDoctorProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterDoctorProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
