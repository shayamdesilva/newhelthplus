import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { Doctor } from '../doctor';
import { EnrollmentService } from '../enrollment.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-register-doctor-profile',
  templateUrl: './register-doctor-profile.component.html',
  styleUrls: ['./register-doctor-profile.component.css']
})
export class RegisterDoctorProfileComponent implements OnInit {
  isSelectDoctor = false;
  showLoadingIndicator = false;
  userDto = new User(null,'',null,null,'','',null,'',false,'','');
  doctorModel = new Doctor(null,'','','','','','','','','','','','','',null,null,false,null,false,null);

  constructor(private EnrollmentService:EnrollmentService,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  registerDoctor(){

  }

  onChange(evt){

  }

  onSubmit(){
    this.EnrollmentService.registerDoctorAccount(this.doctorModel)
    .subscribe(
      data => {
        if(data.status === 1){
          
          this._snackBar.open("Saved Sucessful", "Doctor Saved and Please verify your email address", {
            duration: 4000,
          });
        }else{
          this._snackBar.open("Saved Faill", data['error']['message'], {
            duration: 4000,
          });
      }


          
      }
    )

  }

}
