export class MedicalBrandNameDTO {
    constructor(
        public id: number,
        public userId: number,
        public clinicId:number,
        public createDate: string,
        public genericmedicineId:number,
        public medicineBrandSettingId:number,
        public supplierId:number,
        public drugTypeId:number,
        public drugStrength:string,
        public unit:number,
        public purchacePrice:number,
        public manufactureDate:string,
        public expiryDate:string,
        public genericName: string
    ){}
}
