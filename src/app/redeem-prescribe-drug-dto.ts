export class RedeemPrescribeDrugDTO {
    constructor(
        public id: number,
        public drug: string,
        public dose: string,
        public frequency: number,
        public numberOfDay: number,
        public quantity: number,
        public redeemQuantity: number,
        public clinicId:number,
        public catagory: string,


    ){}
}
