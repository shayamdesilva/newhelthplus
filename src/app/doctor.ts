import { Patient } from "./patient";
export class Doctor {
    constructor(
        public id: number,
        public firstName: string,
        public lastName: string,
        public dateOfBirth: string,
        public nic: string,
        public gender: string,
        public address: string,
        public phoneNumber: string,
        public slmcNumber: string,
        public emergencyContactName: string,
        public emergencyContactNumber: string,
        public email:string,
        public userName: string,
        public password: string,
        public createdDate: Date,
        public updateDate: Date,
        public isMobileVerify: boolean,
        public adminId: number,
        public isEmailVerify: boolean,
        public patientDTO:Array<Patient>



    ){}

}
