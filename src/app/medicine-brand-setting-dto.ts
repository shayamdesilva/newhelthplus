export class MedicineBrandSettingDTO {
    constructor(
        public id: number,
        public brandName: string,
        public minimumQty: string,
        public clinicId: number,
        public userId: number,
        public medicleGenericId:string,
        public sellingPrice:string,
    ){}
}
