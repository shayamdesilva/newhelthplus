import { Fee } from "./fee";
import { Investigation } from "./investigation";
import { Prescription } from "./prescription";
import { PrescriptionDTO } from "./prescription-dto";

export class Diagnosis {
    constructor(
        public id: number,
        public createDate: Date,
        public patientId: number,
        public presentingComplain: string,
        public examination: string,
        public doctorId : String,
        public prescriptionDTO : Prescription,
        public investigationDTO:Investigation,
        public feeDTO:Fee,
        public doctorSlmc:string,
    ){}
}
