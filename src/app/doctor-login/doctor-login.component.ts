import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthUserService } from 'src/services/auth-user.service';
import { Doctor } from '../doctor';
import { EnrollmentService } from '../enrollment.service';
import { JwtClientService } from '../jwt-client.service';
import { Login } from '../login';

@Component({
  selector: 'app-doctor-login',
  templateUrl: './doctor-login.component.html',
  styleUrls: ['./doctor-login.component.css']
})
export class DoctorLoginComponent implements OnInit {
  @ViewChild('Form') addPropertyForm: NgForm
  userLogin = new Login('','');
  doctorModel = new Doctor(null,'','','','','','','','','','','','','',null,null,false,null,false,null);
  jobCardarray:any =[];
  userDto:any=[];
  authRequest:any=[];
  authRequest1:any=[];
  userLoginData:string;
  constructor(private httpClient: HttpClient,
    private authservices:AuthUserService,
    private router:Router,private serv:JwtClientService,
    private _snackBar: MatSnackBar,
    private EnrollmentService:EnrollmentService) { }

  ngOnInit(): void {
  }

  onSubmit(Form :NgForm){
    //Call api and iterate the response
    console.log('Login PW : '+this.userLogin.password);
    console.log('Login username : '+this.userLogin.userName);
    this.doctorModel.email = this.userLogin.userName.toString();
    this.doctorModel.password = this.userLogin.password.toString();

    this.EnrollmentService.doctorLogin(this.doctorModel)
    .subscribe(
      data => {
        if(data.status === 1  && data.httpCode === 200 ){
          // console.log('Status '+data.status);
          this._snackBar.open("Loggin", "Login Sucefull", {
            duration: 2000,
          });
          // console.log(data.userDTos[0]['id'])

          localStorage.setItem('token',data['userDTos'][0]['firstName']);
          localStorage.setItem('tokenid',data['userDTos'][0]['id']);
          // localStorage.setItem('clinicId',data['userDTO']['clinicId']);
          // if(data['rollDTO']['roleName'])
          // localStorage.setItem('slmc',data.userDTos[0]['slmcNumber']);
          // localStorage.setUser('user',data.userDTos[0]);
          // this.maintaince.setUserModel(data.userDTos[0]);
          this.router.navigate(['/maintenance']);

        }else{
          console.log('Status '+data.status);
          this._snackBar.open("Loggin Fail", data['error']['message'], {
            duration: 2000,
          });
        }

          
      }
    )
  }

}
