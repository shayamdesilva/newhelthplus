

export class User {
    constructor(
        public id: number,
        public userName: string,
        public clinicId: string,
        public roleId: number,
        public firstName: string,
        public lastName: string,
        public createDate: Date,
        public password:string,
        public isDoctor:boolean,
        public slmcNumber:string,
        public role:string


    ){}

}
