export class Patient {

    constructor(
        public id: number,
        public firstName: string,
        public lastName: string,
        public dateOfBirth: string,
        public nic: string,
        public gender: string,
        public address: string,
        public phoneNumber: string,
        public emailAddress: string,
        public emergencyContactName: string,
        public emergencyContactNumber: string,
        public isOwner:boolean,
        public ownerId:string,
        public isManager:boolean,
        public managerOwnerId:string,
        public isPatient:boolean,
        public createdDate:Date,
        public updateDate:Date,
        public doctorSlmc:string,
        public patientOwnerId:string,
        public doctorId:string,
        public clinicId:string,
        public slmc:string



    ){}
}
