import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EnrollmentService } from '../enrollment.service';
import { User } from '../user';

@Component({
  selector: 'app-create-system-user',
  templateUrl: './create-system-user.component.html',
  styleUrls: ['./create-system-user.component.css']
})
export class CreateSystemUserComponent implements OnInit {
  showLoadingIndicator = false;
  isSelectDoctor = false;
  userDto = new User(null,'',null,null,'','',null,'',false,'','');
  isSelectUser = false;
  constructor(private _snackBar: MatSnackBar,private http:HttpClient,private EnrollmentService:EnrollmentService) { }

  ngOnInit(): void {
    this.userDto.role = 'System_User';
  }

  onChange(event) {
    // this.filter['property'] = event.value;
  this.isSelectUser = true;
  this.userDto.role = event.value;

    console.log(event.value);
    if(event.value == 'Doctor'){
      this.userDto.isDoctor = true
      this.isSelectDoctor = true
    }
    if(event.value == 'System_User'){
      this.userDto.isDoctor = false;
      this.isSelectDoctor = false
    }

}

SaveUser(){
console.log('First'+this.userDto.firstName)
console.log(this.userDto.lastName)
console.log(this.userDto.userName)
console.log(this.userDto.password)
console.log('is doctor'+this.userDto.isDoctor)
console.log('slmc'+this.userDto.slmcNumber)
console.log('role'+this.userDto.role)

  this.userDto.clinicId = localStorage.getItem('clinicId');
  this.EnrollmentService.createSystemUser(this.userDto)
  .subscribe(
    data => {
      if(data.status == 1){
        this._snackBar.open("Saved", "System User Saved", {
          duration: 2000,
        });
      }else{
        this._snackBar.open("Saved Failed", data.error['message'], {
          duration: 2000,
        });
      }
    }
  )

  
  

}




}
