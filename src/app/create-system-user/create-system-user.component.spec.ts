import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSystemUserComponent } from './create-system-user.component';

describe('CreateSystemUserComponent', () => {
  let component: CreateSystemUserComponent;
  let fixture: ComponentFixture<CreateSystemUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateSystemUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSystemUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
